### Styles metadata editor utility

Use case: adapting new styles for usage with microG's custom styles by adding required metadata fields.

Metadata fields edited:

* `microg:gms-type-feature`
* `microg:gms-type-element`

Each is checked against a specific allowlist with microG's custom styles implementation in mind.

How to use:

1. Open in IntelliJ
2. Add a run configuration with the file to be edited as parameter
3. Run the configuration and answer the prompts in the Run tab
4. Once finished, pipe the output file through `jq --indent 4`. The result can be written to `play-services-maps-core-mapbox/src/main/assets`.
