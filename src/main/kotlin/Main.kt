/*
 * style-metadata-editor – edit style metadata for use with microG
 * Copyright (C) 2023 e foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.grack.nanojson.JsonObject
import com.grack.nanojson.JsonParser
import com.grack.nanojson.JsonWriter
import java.io.File

const val KEY_FEATURE_TYPE = "microg:gms-type-feature"
const val KEY_ELEMENT_TYPE = "microg:gms-type-element"

fun main(args: Array<String>) {

    if (args.isEmpty()) {
        println("Please provide the file to be edited as a parameter.")
        return
    }

    val styleTwoLayerArray = if (args.size > 1) {
        println("You have provided a second style at ${args[1]}. Great! I fill match it with the first style by ID and pre-fill the prompts accordingly.")
        JsonParser.`object`().from(File(args[1]).bufferedReader()).getArray("layers")
    } else {
        null
    }

    val style = JsonParser.`object`().from(File(args[0]).bufferedReader())

    // Crash if an invalid style is provided.
    println("Let's work through the style at `${args[0]}` layer-by-layer. Please open the style in mapbox studio to get a better idea of the layers we are looking at.")
    println()
    val layers = style.getArray("layers")

    val inReader = System.`in`.bufferedReader()

    for (i in 0 until layers.size) {
        val layer = layers.getObject(i)
        val id = layer.getString("id")
        println("Layer: $id")

        layer.putIfAbsent("metadata", JsonObject())
        val metadata = layer.getObject("metadata")!!

        val styleTwoMatchingLayerMetadata =
            (styleTwoLayerArray?.find { it is JsonObject && it.getString("id") == id && it.has("metadata") }
                    as JsonObject?)?.getObject("metadata")

        // Query new feature type from user
        do {

            val prefill = if (styleTwoMatchingLayerMetadata?.has(KEY_FEATURE_TYPE) == true) styleTwoMatchingLayerMetadata.getString(KEY_FEATURE_TYPE)
                else if (metadata.has(KEY_FEATURE_TYPE)) metadata.getString(KEY_FEATURE_TYPE) else null

            if (prefill != null) {
                print("Feature type [$prefill]? ")
            } else {
                print("Feature type? ")
            }

            val newFeatureType = inReader.readLine()
            if (newFeatureType.isEmpty() && prefill in FEATURE_TYPE_ALLOWLIST) {
                metadata[KEY_FEATURE_TYPE] = prefill
                break
            } else {
                if (newFeatureType !in FEATURE_TYPE_ALLOWLIST) {
                    println("Invalid feature type! Allowed feature types: $FEATURE_TYPE_ALLOWLIST")
                } else {
                    metadata[KEY_FEATURE_TYPE] = newFeatureType
                    break
                }
            }
        } while (true)


        // Query new element type from user
        do {
            val prefill = if (styleTwoMatchingLayerMetadata?.has(KEY_ELEMENT_TYPE) == true) styleTwoMatchingLayerMetadata.getString(KEY_ELEMENT_TYPE)
                else if (metadata.has(KEY_ELEMENT_TYPE)) metadata.getString(KEY_ELEMENT_TYPE) else null

            if (prefill != null) {
                print("Element type [$prefill]? ")
            } else {
                print("Element type? ")
            }

            val newElementType = inReader.readLine()
            if (newElementType.isEmpty() && prefill in ELEMENT_TYPE_ALLOWLIST) {
                metadata[KEY_ELEMENT_TYPE] = prefill
                break
            } else {
                if (newElementType !in ELEMENT_TYPE_ALLOWLIST) {
                    println("Invalid element type! Allowed element types: $ELEMENT_TYPE_ALLOWLIST")
                } else {
                    metadata[KEY_ELEMENT_TYPE] = newElementType
                    break
                }
            }
        } while (true)

        println()
    }

    println()

    JsonWriter.indent("    ").on(
        File(args[0].plus(".out")).bufferedWriter()
    ).value(style).done()
    println("Okay, that's it! I've written your edited style to `${args[0]}.out`. You may wish to reformat it by piping it through `jq --indent 4`.")
}

val FEATURE_TYPE_ALLOWLIST = listOf(
    "administrative.country", "administrative.land_parcel", "administrative.locality", "administrative.neighborhood", "administrative.province",
    "landscape.man_made", "landscape.natural.landcover", "landscape.natural.terrain",
    "poi", "poi.attraction", "poi.business", "poi.government", "poi.medical", "poi.park", "poi.place_of_worship", "poi.school", "poi.sports_complex",
    "road", "road.arterial", "road.highway", "road.highway.controlled_access", "road.local", "transit.line", "transit.station.airport", "transit.station.bus", "transit.station.rail",
    "water"
)
val ELEMENT_TYPE_ALLOWLIST = listOf(
    "geometry.fill", "geometry.stroke",
    "labels.icon", "labels.text"
)
